from tkinter import *
from tkinter import messagebox
class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.resultField = Text(master, bg='#FFFFFF', fg='#000000', height=5)
        self.resultField.insert(INSERT,"0")
        self.resultField.grid(row=0, column=5)
        bDot = Button(master, text='.', command=lambda: self.notice('.'), height=2, width=6)
        b1=Button(master, text='1', command=lambda: self.notice(1), height=2, width=6)
        b2=Button(master, text='2', command=lambda: self.notice(2), height=2, width=6)
        b3=Button(master, text='3', command=lambda: self.notice(3), height=2, width=6)
        bPlus = Button(master, text='+',command=lambda: self.notice("+"), height=2, width=6)
        b4 = Button(master, text='4', command=lambda: self.notice(4), height=2, width=6)
        b5 = Button(master, text='5', command=lambda: self.notice(5), height=2, width=6)
        b6 = Button(master, text='6', command=lambda: self.notice(6), height=2,width=6)
        bMinus = Button(master,text='-', command=lambda: self.notice("-"), height=2, width=6)
        b7 = Button(master, text='7', command=lambda: self.notice(7), height=2, width=6)
        b8 = Button(master, text='8', command=lambda: self.notice(8), height=2, width=6)
        b9 = Button(master, text='9', command=lambda: self.notice(9), height=2, width=6)
        bMultiply = Button(master, text='*',command=lambda: self.notice("*"), height=2, width=6)
        b0 = Button(master, text='0', command=lambda: self.notice(0), height=2, width=6)
        bLeft = Button(master, text='(', command=lambda: self.notice('('), height=2, width=6)
        bRight = Button(master, text=')', command=lambda: self.notice(')'), height=2, width=6)
        bDivide = Button(master, text='/', command=lambda: self.notice('/'), height=2, width=6)
        bCalculate = Button(master, text="=", command=self.displayRes, height=2,width=6)
        bClear = Button(master, text="Clear", command=self.clear, height=2, width=6)
        bPercentage = Button(master, text='%', command=self.clear, height=2,width=6)

        bClear.grid(row=5, column=0)
        bCalculate.grid(row=5, column=1)
        bLeft.grid(row=5, column=2)
        bRight.grid(row=5, column=3)
        bDot.grid(row=4, column=0)
        b0.grid(row=4,column=1)
        bPercentage.grid(row=4,column=2)
        bMinus.grid(row=4, column=3)
        b1.grid(row=3,column=0)
        b2.grid(row=3,column=1)
        b3.grid(row=3,column=2)
        bPlus.grid(row=3,column=3)
        b4.grid(row=2,column=0)
        b5.grid(row=2,column=1)
        b6.grid(row=2,column=2)
        bDivide.grid(row=2, column=3)
        b7.grid(row=1,column=0)
        b8.grid(row=1,column=1)
        b9.grid(row=1,column=2)
        bMultiply.grid(row=1,column=3)

    def notice(self,num):
        if self.resultField.get("0.0", END)=="0\n":
            self.resultField.delete("0.0",END)
        self.resultField.insert(INSERT, str(num))
    def clear(self):
        self.resultField.delete("0.0",END)
        self.resultField.insert(INSERT,"0")
    def displayRes(self):
        res = self.calculate(self.resultField.get("0.0",END)[:-1])
        self.resultField.delete("0.0",END)
        self.resultField.insert(INSERT,str(res))
    def calculate(self,task):
        if task=="ERROR":
            return "ERROR"
        try:
            return(float(task))
        except ValueError:
            if")"in task:
                level=0
                maxLevelStartIndex=0
                maxLevelEndIndex=0
                for i in range(0,len(task)):
                    if task[i]=="(":
                        level+=1
                        maxLevelStartIndex=i

                    if task[i]==")":
                        level-=1
                if level!=0:
                    print("ERROR: brackets don't match:%i layers too much in expression %s" %(level, task))
                    return "ERROR"
                for i in range(maxLevelStartIndex, len(task)):
                    if task[i]==")":
                        maxLevelEndIndex=i
                        break
                    newTask = task[:maxLevelStartIndex]+str(self.calculate(task[maxLevelStartIndex+1:maxLevelEndIndex]))+task[maxLevelEndIndex+1:]
                    return self.calculate(newTask)
            elif "+" in task:
                tesk = task.split("+")
                res = self.calculate(tesk[0])
                for i in tesk[1:]:
                    res+=self.calculate(i)
                return res
            elif "-" in task:
                tesk = task.split("-")
                res = self.calculate(tesk[0])
                for i in tesk[1:]:
                    res -=self.calculate(i)
                return res
            elif "*" in task:
                tesk = task.split("*")
                res = self.calculate(tesk[0])
                for i in tesk[1:]:
                    res*=self.calculate(i)
                return res
            elif "/" in task:
                tesk = task.split("/")
                res = self.calculate(tesk[0])
                for i in tesk[1:]:
                   try:
                       res /=self.calculate(i)
                   except ZeroDivisionError:
                        print("ERROR:division by 0")
                        return "ERROR"
                return res
            else:
                messagebox.showerror("ERROR", "ERROR: invalid expression")
                return "ERROR"
root = Tk()
app = Window(root)
root.wm_title("Calculator")
root.geometry('700x350+50+50')
root.mainloop()
